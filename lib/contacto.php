<!DOCTYPE html>
<html lang="es">
  <head>
        <link href="../css/bootstrap.css" rel="stylesheet">
  </head>
  <body>

<?php
require_once 'mailer/class.phpmailer.php';

if($_POST['subject']==''){
    
    echo $_POST['subject'];
    echo 'Error al Enviar Mensaje';
    
}else{

    // * --------------------
    
    $mail            = new phpmailer();
    $mail->PluginDir = "mailer/";
    
    // * --------------------
        
    $mail->IsSMTP();
    $mail->Host      = 'ssl://smtp.gmail.com';
    $mail->Port      = 465;
    $mail->SMTPAuth  = true;
    $mail->Username  = "info@sanchezsolutions.com.ve";
    $mail->Password  = "06211q2w3e4rss";
    
    
    // * --------------------
    
    $sendTo          = strtolower("styp152@gmail.com");
    $mail->AddAddress($sendTo);
    
    $mail->From      = 'info@sanchezsolutions.com.ve';
    $mail->ConfirmReadingTo = 'info@sanchezsolutions.com.ve';
    $mail->FromName  = utf8_decode('Sánchez Solutions'); 
    
    // * --------------------
    
    $mail->IsHTML(false);
    $mail->Subject   = "[styp152.com.ve] {$_POST['subject']}";
    
    $mail->Body      = "\n\nÉste es un mensaje automático para notificarte que alguien se comunica contigo en la web.\n\n";
    $mail->Body     .= "Los datos de comunicacion son:\n\n";
    $mail->Body     .= "Nombre  = {$_POST['name']}\n";
    $mail->Body     .= "Correo  = {$_POST['email']}\n";
    $mail->Body     .= "Telefono = {$_POST['phone']}\n\n";
    $mail->Body     .= "Mensaje = {$_POST['message']}\n\n";
    
    $mail->Body     .= "Ante cualquier duda, comuniquese con el administrador del sitio.\n\n";
    $mail->Body     .= "--\n";
    $mail->Body     .= "www.sanchezsolutions.com.ve\n";
    
    // * --------------------
    
    if (!$mail->Send()):
    ?>
      <div class="alert alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>Lo siento Dave, me temo que no puedo hacer eso.</h4>
        <p>Ocurrió un problema al enviar el mensaje! <?=$mail->ErrorInfo?>.</p>
        <br />
        <button type="button" data-dismiss="alert" aria-hidden="true" class="btn btn-danger">Aceptar</button>
      </div>
    <?php else: ?>
      <div class="alert alert-success fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>Es un pequeño paso para el hombre, pero un gran salto para mantenernos en Contacto.</h4>
        <p>Hemos registrado tu solicitud exitosamente, en breve nos comunicaremos contigo.</p>
        <br />
        <button type="button" data-dismiss="alert" aria-hidden="true" class="btn btn-success">Aceptar</button>
      </div>
    <?php
    endif;

}
    
?>

<script src="../js/jquery-1.10.2.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(".alert").alert()
    $('.alert').on('closed.bs.alert', function (e) {
        location.href = '../index.html'
    })
</script>

</body>
</html>